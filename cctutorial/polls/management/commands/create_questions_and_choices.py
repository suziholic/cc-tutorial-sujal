import random

from django.core.management.base import BaseCommand
from polls.models import Question, Choice


class Command(BaseCommand):
    help = 'Create 4 Questions and 3-6 Choices for each of those Question objects'

    def handle(self, *args, **kwargs):
        # use the Django API models to create four new questions in the
        # database as well as 3-6 Choices/Answers for those questions.
        # See "Playing with the API" in the Django Tutorial:
        # https://docs.djangoproject.com/en/1.8/intro/tutorial01/
        #
        # When creating your choices, assign a random number of votes to
        # that choice with a value between 0 and 10.
        questions = [
            "What",
            "Where",
            "When",
            "Why"
        ]
        for question_text in questions:
            question = Question.objects.create(question_text=question_text)
            self._set_choices(question)


    def _set_choices(self, question):
        choice_mapper = {
            "What": ["Thinking", "Nothing", "Doing", "Not much"],
            "Where": ["Anywhere", "Everywhere", "Nowhere", "Somewhere", "The sky"],
            "When": ["Sometime", "Anytime", "Notime"],
            "Why": ["Whyone", "Whytwo", "Whythree", "WhyFour", "Whyfive", "WhySix"]
        }
        choices = choice_mapper.get(question.question_text)
        choice_create = question.choice_set.create
        [
            choice_create(choice_text=choice,
            votes=random.choice(range(1, 11)))
            for choice in choices
        ]
