import datetime
from django.http import HttpResponse
from django.template.loader import render_to_string
from django.shortcuts import render_to_response, get_object_or_404
from django.db.models import Max
from polls.models import Choice, Question


def index(request):
    # latest_question_list = Question.objects.only('question_text').order_by('-pub_date')[:5]
    latest_question_list = Question.objects.order_by('-pub_date')
    # latest_question_list = latest_question_list.values_list("question_text", "pub_date")  # 11 Point

    # Following change for point 14
    context = {"latest_question_list": latest_question_list[:5]}
    return render_to_response("poll_index.html", context=context)

def most_popular_choice(request):
    """
    After you have created a series of questions and choices,
    return the choice with the most votes of all choices. If there
    are multiples with the same number of votes, return al. For
    the choice, display the choice_text, number of votes and
    related Question id in the response below.
    """
    max__vote = Choice.objects.aggregate(Max('votes'))
    choices = Choice.objects.filter(votes=max__vote["votes__max"])
    context = {'choices': choices}
    return render_to_response("choices.html", context=context)


def choices(request, pk=None):
    """
    If a primary key is passed in, return only that choice object.
    If no primary key is passed in, return all choices ordered by highest number of votes first.

    Show the related question in both instances
    """
    context = None
    if pk:
        # choice = Choice.objects.get(pk=pk)
        choice = get_object_or_404(Choice, pk=pk)
        context = {'choices': [choice], "pk": pk}
    else:
        choices = Choice.objects.all().order_by('pk')
        # Following will cover 8th point from to do list.
        choices_question_ids = choices.values_list('question', flat=True).distinct()
        questions = Question.objects.filter(id__in=choices_question_ids)
        # questions = []
        # for choice in choices:
        #     Follwoing 7 point from to do list.
        #     question = get_object_or_404(Question, pk=choice.question_id)
        #     questions.append(question)
        context = {'choices': choices, 'questions': questions}
    return render_to_response("choices.html", context=context)


def question(request, pub_date=datetime.date.today()):
    """
    If a primary key is passed in, return only that choice object.
    If no primary key is passed in, return all choices ordered by highest number of votes first.

    Show the related question in both instances
    """
    if isinstance(pub_date, unicode):
        pub_date = datetime.datetime.strptime(pub_date, r"%Y-%m-%d")
        questions = Question.objects.filter(pub_date__gte=pub_date)
    else:
        questions = Question.objects.filter(pub_date=pub_date)
    context = {"questions": questions, "pub_date": pub_date}
    return render_to_response("questions.html", context=context)
