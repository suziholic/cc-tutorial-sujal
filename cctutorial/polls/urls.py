from django.conf.urls import url

from django.views.generic.base import TemplateView
from polls import views

urlpatterns = [
    url(r'^$', views.index, name='polls_index'),
    url(r'most-popular/$', views.most_popular_choice, name='most-popular-choice'),
    url(r'choices/(?P<pk>[0-9])/$', views.choices, name='choices'),
    url(r'choices/$', views.choices, name='choices'),
    url(r'question/(?P<pub_date>[0-9]{4}[-/][0-9]{2}[-/][0-9]{2})/$', views.question, name='question'),
    url(r'question/$', views.question, name='question'), # 9th point from todo list
]
