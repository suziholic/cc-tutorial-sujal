from django.test import TestCase
from django.core.urlresolvers import reverse


# Create your tests here.
class PollsTests(TestCase):
    """
    Simple tests for the polls app
    """

    def test_choices(self):
        """Test the choices view"""

        response = self.client.get(reverse('choices'))        
        self.assertEqual(response.status_code, 200)

    def test_choices_for_non_existant_id(self):
    # Add a test for a non-existant ID and test for the expected result
        response = self.client.get(reverse('choices', kwargs={'pk': 8}))
        self.assertEqual(response.status_code, 404)
        self.assertNotEqual(response.status_code, 200)

    def test_most_popular_choice(self):
        response = self.client.get(reverse('most-popular-choice'))
        self.assertEqual(response.status_code, 200)
